
#include "notepad.h"
#include "headerFiles.h"
#include "userAuthentication.pb.h"
#include "projectDetails.pb.h"

const char* currentUser;
unique_ptr<user::userObj> obj = make_unique<user::userObj>();
user::userObj users;
unique_ptr<projectData::projectDetails> projectDetObj = make_unique<projectData::projectDetails>();

void Notepad::common::signUP()
{
    cout << "\nEnter Registration Details for User  \n";
    obj->registerUser(users);
    cout << "\nRelogin to handle active operations\n";
    Notepad::common::signIN();

}
void Notepad::common::signIN() {
    char newUserName[10], newPassword[10];
    cout << "\nEnter user name :: ";
    cin >> newUserName;
    cout << "\nEnter password :: ";
    cin >> newPassword;
    if (obj->loginUser(newUserName, newPassword) == 1)
    {
        currentUser = newUserName;
        if (obj->checkAdmin(newUserName) == 1)
        {
            cout << "Welcome Admin - " << newUserName << "\n";
            userMenu(newUserName, "admin");
        }
        else {
            cout << "Welcome User - " << newUserName << "\n";
            userMenu(newUserName, "user");
        }
    }

    else
    {

        if (obj->checkUserName(newUserName) == 1) {
            cout << "\Wrong Password\nRe-Enter Details\n";
            signIN();
        }
        else {
            cout << "\nNew User, Do create an account before Logging in,\n";
            signUP();
        }
        

    }
}

void Notepad::common::userMenu(char current[20], const char* userType) {
    int choice;
    char ch;
    currentUser = current;
    do {
        if (userType == "admin") {
            cout << "\n<-------------- Menu ------------->\n\n";

            cout << "1.Create Project\n2.Append Data to a Project\n3.Update an existing Project\n4.Remove data from existing Project\n5.Display Project Contents\n6.Revert to a version of Project\n7.Details of Project\n8.Admin Log\n9.My Dashboard\n\n";
            cout << "Enter your choice: ";
            cin >> choice;
            switch (choice) {
            case 1:projectDetObj->createProject(current);break;
            case 2: projectDetObj->extendProject(current);break;
            case 3: projectDetObj->updateContent(current);break;
            case 4:projectDetObj->removeContent(current);break;
            case 5: projectDetObj->displayContents(current); break;
            case 6: projectDetObj->revertVersion(current); break;
            case 7: projectDetObj->projectInfo(current); break;
            case 8: obj->adminLog();break;
            case 9: projectDetObj->userDashboard(current);break;

            default: cout << "Invalid choice"; break;
            }

        }
        else {
            cout << "\n...........Menu.......... \n\n1.Create Project\n2.Append Data to a Project\n3.Update an existing Project\n4.Remove data from existing Project\n5.Display Project Contents\n6.Revert to a version of Project\n7.Details of Project\n8.My Dashboard\n\n";
            cout << "Enter your choice: ";
            cin >> choice;
            switch (choice) {
            case 1:projectDetObj->createProject(current);break;
            case 2: projectDetObj->extendProject(current);break;
            case 3: projectDetObj->updateContent(current);break;
            case 4:projectDetObj->removeContent(current);break;
            case 5: projectDetObj->displayContents(current); break;
            case 6: projectDetObj->revertVersion(current); break;
            case 7: projectDetObj->projectInfo(current); break;
            case 8: projectDetObj->userDashboard(current);break;
            default: cout << "Invalid choice"; break;
            }
        }

        cin.get();
        cout << "\nDo you wish to continue with the notepad operations " << " (y/n)? :: ";
        cin >> ch;
    } while (ch == 'y' || ch == 'Y');

}





int main()
{
    int choice;
    char ch;
    /*user::userObj objj;   //to add admin user
    objj.addAdmin();*/
    cout << "\n<--------------------------------------------------------------------------------------------------->\n";
    cout << "                                            Notepad Application                                        \n";
    cout << "<--------------------------------------------------------------------------------------------------->\n";
    cout << "\nWelcome to the menu driven Notepad Application where you can create, append, update and delete projects!!\n";

    do {
        cout << "\nEnter details to log in\n";
        Notepad::common::signIN();
        cout << "\nDo you wish to continue working on Notepad Project (y/n)? :: ";
        cin >> ch;
    } while (ch == 'y' || ch == 'Y');

    cin.get();

    
    return 0;
}



