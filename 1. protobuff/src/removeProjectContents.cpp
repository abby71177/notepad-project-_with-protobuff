#include "projectDetails.pb.h"
#include "Notepad.h"

void projectData::projectDetails::removeContent(char* directory) {                 //to remove contents of project
    int choice;
    string projectName;
    cout << "\nTo remove contents of a project, Enter\n1.Remove contents of file completely\n2.To remove particular line\nenter choice :: ";
    cin >> choice;
    cin.get();

    cout << "Enter Project name :: ";
    getline(cin, projectName);
    string path("");

    path = Notepad::common::returnFilepath(directory, projectName, 1);
    int result = Notepad::common::checkFile(path);
    if (result==1)
    {
        if (choice == 1)
        {
            ofstream ofs;
            ofs.open(path, ofstream::out | ofstream::trunc);
            ofs.close();
            cout << "\nContents of file " << projectName << " were removed\n";
            updateOperations(projectName, directory, 1);
            checkVersion(projectName, directory);
        }
        else {
            ifstream fin;
            fin.open(path);

            string line;
            int lineNo;
            //cin.get();
            cout << "\nenter the line number which you want to remove :: ";
            cin >> lineNo;
            string path2("");
            path2 += directory;
            path2 += "/";
            path2 += "temp.txt";
            ofstream temp;
            temp.open(path2);
            int lineCount = 1;
            while (getline(fin, line) && fin) {
                if (lineCount != lineNo)
                    temp << line << "\n";
                lineCount++;
            }
            if (lineNo >= lineCount || lineNo < 0)
                cout << "\nRequested line is not present in the file\n";
            else
            {
                cout << "\nRequested line was removed from the file\n";
                updateOperations(projectName, directory, 1);
                checkVersion(projectName, directory);
            }


            temp.close();
            fin.close();
            char newFile[40], oldFile[40];
            strcpy_s(oldFile, path.c_str());
            strcpy_s(newFile, path2.c_str());
            remove(oldFile);
            rename(newFile, oldFile);


        }
    }
    else
        cout << "No such file exists\n";




}
