#include "projectDetails.pb.h"
#include "userAuthentication.pb.h"

int projectData::projectDetails::returnOperationCount(string projName, string user, int item) {         //to return either noOfOperations or no of versions od a project
    projectDetails project_;
    fstream input("project.dat", ios::in | ios::binary);
    if (!project_.ParseFromIstream(&input))
        cerr << "Failed to parse address book." << endl;
    else
    {
        for (int index = 0;index < project_.projects_size();index++)
        {
            project obj = project_.projects(index);
            if (projName == obj.projectname() && user == obj.username())
            {
                switch (item)
                {
                case 1: return obj.no_of_operations(); break;
                case 2: return obj.no_of_versions(); break;
                }
            }
        }
    }
    input.close();
}

void projectData::projectDetails::updateOperations(string name, string user, int variable) {      //variable=1 to increment operations and 2 to increment version
    
    projectDetails project_2;
    { 
        fstream input("project2.dat", ios::in | ios::binary);
        if (!project_2.ParseFromIstream(&input))
            cerr << endl;
    }
    projectDetails project_;
    fstream input("project.dat", ios::in | ios::binary);
    if (!project_.ParseFromIstream(&input)) {
        cerr << "Failed to parse  book (project.dat)." << endl;
    }
    else {
        for (int index = 0;index < project_.projects_size();index++)
        {
            project obj_ = project_.projects(index);
            project* obj_2 = project_2.add_projects();
            if (user == obj_.username() && name == obj_.projectname()) {
                switch (variable)
                {
                case 1: obj_2->set_no_of_operations(obj_.no_of_operations() + 1);
                    obj_2->set_no_of_versions(obj_.no_of_versions());
                    break;
                case 2: obj_2->set_no_of_versions(obj_.no_of_versions() + 1);
                    obj_2->set_no_of_operations(obj_.no_of_operations());
                    break;

                }
            }
            else {
                obj_2->set_no_of_versions(obj_.no_of_versions());
                obj_2->set_no_of_operations(obj_.no_of_operations());
            }
            obj_2->set_id(obj_.id());
            obj_2->set_projectname(obj_.projectname());
            obj_2->set_username(obj_.username());
            fstream output("project2.dat", ios::out | ios::trunc | ios::binary);
            if (!project_2.SerializeToOstream(&output))
                cerr << "Failed to write address book." << endl;

        }
        input.close();
        remove("project.dat");
        rename("project2.dat", "project.dat");
    }

}
            







