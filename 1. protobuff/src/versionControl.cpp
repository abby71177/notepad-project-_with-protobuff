#include "projectDetails.pb.h"
#include"Notepad.h"


void projectData::projectDetails::versionLog(string Id, string projName, string user) {
    version *versionObj;
    projectDetails project_;
    {
        fstream input("version.dat", ios::in | ios::binary);
        if (!project_.ParseFromIstream(&input)) {
            cerr << "Failed to parse  book." << endl;
        }
    }
    versionObj = project_.add_versions();
    versionObj->set_versionid( Id);
    versionObj->set_projectname(  projName);
    versionObj->set_username( user);
    // Write the new address book back to disk.
    fstream output("version.dat", ios::out | ios::trunc | ios::binary);
    if (!project_.SerializeToOstream(&output)) {
            cerr << "Failed to write into book(verson.dat)." << endl;
        }
    
    

}
void projectData::projectDetails::displayVersions(int choice, string userName, string projectName) {
    if (choice == 1)
    {
        int found = 0;
        projectDetails project_;

        fstream input("version.dat", ios::in | ios::binary);
        if (!project_.ParseFromIstream(&input))
            cerr << "Failed to parse book." << endl;
        else
        {
            for (int index = 0;index < project_.versions_size();index++) {
                version versionObj = project_.versions(index);
                if (versionObj.username() == userName)
                {
                    found = 1;
                    cout << "  Version Id :: " << versionObj.versionid() << endl;
                }
            }
            if (found == 0)
                cout << "No versions are created for the requested user\n";

        cout << "\n<-------------------------------->\n\n";

        }
    }
    else if (choice == 2) {
        int found = 0;
        projectDetails project_;

        fstream input("version.dat", ios::in | ios::binary);
        if (!project_.ParseFromIstream(&input))
            cerr << "Failed to parse book." << endl;
        else
        {
            for (int index = 0;index < project_.versions_size();index++) 
            {
                version versionObj = project_.versions(index);
                if (versionObj.projectname() == projectName && versionObj.username() == userName)
                {
                    found = 1;
                    cout << "  Version Id :: " << versionObj.versionid() << endl;
                }
            }
            if (found == 0)
                cout << "\nNo versions are created for this project ";
        }

                 cout << "\n<-------------------------------->\n\n";

        }

}


void projectData::projectDetails::checkVersion(string projName, char* directory) {                //to check if version of a project is to be created
    int operation = returnOperationCount(projName, directory, 1);
    if (operation % 10 == 0)
    {

        cout << "\n<------- Version of project is created!! ------->\n";
        string path, line, id;
        path = Notepad::common::returnFilepath(directory, projName, 4);
        path += to_string(operation / 10);
        path += ".txt";
        id += projName;
        id += "-V";
        id += to_string(operation / 10);
        cout << "Version Id :: " << id << "\n";
        ifstream fin;
        fin.open(Notepad::common::returnFilepath(directory, projName, 1));
        ofstream fout;
        fout.open(path);
        while (getline(fin, line)) {

            fout << line << "\n";
        }
        updateOperations(projName, directory, 2);
        versionLog(id, projName, directory);
        fout.close();
        fin.close();


    }
}

void projectData::projectDetails::revertVersion(char* user) {              //to revert to a version of project
    int versionNo;
    string file, path, path2;
    cin.get();
    cout << "\nEnter the Project name you want to revert :: ";
    getline(cin, file);
    path = Notepad::common::returnFilepath(user, file, 1);
    cout << path << endl;
    cout << "\nEnter the version number you want to revert the project back to :: ";
    cin >> versionNo;
    int currentVersion = returnOperationCount(file, user, 2);
    if (versionNo > currentVersion)
        cout << "\nInvalid Version Number/ Version not yet created\n";
    else {

        path2 = Notepad::common::returnFilepath(user, file, 4);
        path2 += to_string(versionNo);
        path2 += ".txt";
        cout << path2;
        ofstream ofs;
        ofs.open(path, ofstream::out | ofstream::trunc);
        ofs.close();
        string line;
        ifstream fin;
        fin.open(path2);
        ofstream temp;
        temp.open(path);
        if (!fin || !temp)
            cout << "\nNo such file is exists\n";
        else {
            getline(fin, line);
            while (fin) {
                temp << line << "\n";
                getline(fin, line);
            }
            fin.close();
            temp.close();

            cout << "\nRevertion successfully executed\n";

            updateOperations(file, user, 1);
            checkVersion(file, user);

        }

    }
}
