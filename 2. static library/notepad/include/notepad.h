#pragma once

#include"headerFiles.h"

using namespace std;
namespace Notepad {
	namespace common {
		void createDirectory(string first, string projectName, int choice);  //to create directory on user registration
		string returnFilepath(string userName, string projectName, int choice); //1 to return data.txt and 2 to retutn version file
		string fileInput();                      //to write data into file
		void signIN();                          //for user SIGN IN
		void userMenu(char[], const char*);     //to display userMenu and adminMenu
		void signUP();                         //for user SIGNUP
		bool checkFile(string);       //to check if file exists



	}
}

