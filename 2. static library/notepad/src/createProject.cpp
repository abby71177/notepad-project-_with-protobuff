#include "projectDetails.pb.h"
#include "userAuthentication.pb.h"
#include "Notepad.h"


char* CurrentUser;
user::userObj userObj;
user::userDetails obj;
projectData::projectDetails projObj;
projectData::project projects;



int projectData::projectDetails::returnProjectCount(string userName) {             //to return no of projects created so far
    user::userObj users_;
    user::userDetails obj;
    int flag = 0;
    fstream input("userDetails.dat", ios::in | ios::binary);

    if (!users_.ParseFromIstream(&input)) {
        cerr << "Failed to parse user Details File" << endl;
        return -1;
    }
    else
    {
        for (int index = 0;index < users_.users_size();index++)
        {
            user::userDetails obj = users_.users(index);


            if (obj.username() == userName) {
                flag = 1;
                return obj.no_of_projects();
            }

        }

    }
    if (flag == 0) return 0;
}

void projectData::projectDetails::updateProjectCount(string uName, int projectCount) {  //1->no_of_projects 2->operation count 3->version count
    user::userObj user_;
    user::userObj user_2;
    //user_2-read
    projectCount++;
    {
        fstream input_("userDetails2.dat", ios::in | ios::binary);
        if (!user_2.ParseFromIstream(&input_)) {
           // cerr << "Failed to parse address book." << endl;
        }
        input_.close();

    }

    fstream input("userDetails.dat", ios::in | ios::binary); //read
    if (!user_.ParseFromIstream(&input))
        cerr << "Failed to parse address book." << endl;
    else
    {
        for (int index = 0;index < user_.users_size();index++)
        {
            user::userDetails obj = user_.users(index);  //to read
            user::userDetails* obj_2 = user_2.add_users(); //to write

            if (obj.username() == uName)
                obj_2->set_no_of_projects(projectCount);

            else
                obj_2->set_no_of_projects(obj.no_of_projects());


            obj_2->set_name(obj.name());
            obj_2->set_email(obj.email());
            obj_2->set_location(obj.location());
            obj_2->set_username(obj.username());
            obj_2->set_password(obj.password());
            obj_2->set_admin(obj.admin());
            /*{
                cout << "\nUser Name :" << obj_2->username() << endl;

                cout << "\nPassword :" << obj_2->password();
                cout << "\nName :" << obj_2->name();
                cout << "\nEmail :" << obj_2->email();
                cout << "\nLocation :" << obj_2->location();
                cout << "\nNo of projects : " << obj_2->no_of_projects();
            }*/
            fstream  output("userDetails2.dat", ios::out | ios::trunc | ios::binary); //write
            if (!user_2.SerializeToOstream(&output))
                cerr << "Failed to open user details file." << endl;
        }
        input.close();
        remove("userDetails.dat");
        rename("userDetails2.dat", "userDetails.dat");

    }


}









void projectData::projectDetails::createProject(char* directory) {           // to create a project for user
    int stop;
    ofstream filout;
    string path(""), path2;
    string tempId;
    projectData::projectDetails projObj;
    {
        fstream input("project.dat", ios::in | ios::binary);
        if (!input) {
            cout << ": File not found.  Creating a new file." << endl;
        }
        else if (!projObj.ParseFromIstream(&input)) {
            //cerr << "Failed to parse address book." << endl;
        }
    }


    CurrentUser = directory;
    int projectCount = returnProjectCount(directory);
    updateProjectCount(directory, projectCount);


    string projectName = "Project ";
    projectCount++;
    projectName += to_string(projectCount);

    tempId = "";
    tempId += directory;
    tempId += to_string(projectCount);
    cout << endl << projectName << " is created whose Id is " << tempId << "\n";
    Notepad::common::createDirectory(directory, projectName, 2);
    path2 = Notepad::common::returnFilepath(directory, projectName, 1);
    //cout << endl << endl << path2;
    ofstream MyFile(path2);
    cin.get();
    cout << "Enter 1 to keep adding sentences to the file and 0 to terminate\n";
    do {
        MyFile << Notepad::common::fileInput();
        cin >> stop;
        cin.get();

    } while (stop != 0);
    MyFile.close();
    projectData::project* projects_;

    projects_ = projObj.add_projects();

    projects_->set_id(tempId);
    projects_->set_no_of_operations(0);
    projects_->set_no_of_versions(0);
    projects_->set_username(directory);
    projects_->set_projectname(projectName);



    fstream output("project.dat", ios::out | ios::trunc | ios::binary);
    if (!projObj.SerializeToOstream(&output)) {
        cerr << "Failed to parse the project Details book." << endl;
    }
    //cin.get();
}

