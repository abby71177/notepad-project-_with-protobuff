
Creating static library for the notepad project using cmake.
<br>
Contents of the project are as follows
<ul>
<li> notepad
<ul>
<li>bin  - consists  the .lib file of the project</li>
<li> include 
<ul>
<li>HeaderFiles.h</li>
<li>notepad.cpp</li>
<li>projectDetails.pb.h</li>

<li>userAuthentication.pb.h</li>
</li>
<li>src
<ul>
<li>addAdminData.cpp</li>
<li>adminOps.cpp</li>
<li>appendProject.cpp</li>
<li>createProject.cpp</li>
<li>displayContents.cpp</li>
<li>main.cpp</li>
<li>notepad.cpp</li>
<li>projectDetails.cpp</li>

<li>removeProjectContent.cpp</li>
<li>updateProjectContent.cpp</li>
<li>updateProjectDetails.cpp</li>
<li>userAuthentication.cpp</li>
<li>versionControl.cpp</li>

</li>
<li> CMakeLists.txt  - cmake confirguration file</li>
