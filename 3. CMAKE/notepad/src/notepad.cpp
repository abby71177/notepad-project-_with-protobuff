#include"notepad.h"


void Notepad::common::createDirectory(string userName, string projectName, int choice) { //username,projects,projectname
	string path;
	char newFile[20], oldFile[30], tempFile[40];
	path = userName;
	path += "/";
	path += "projects";
	strcpy_s(newFile, path.c_str());
	_mkdir(newFile);

	if (choice == 2) {   //project directory with version
		path += "/";
		path += projectName;
		string path2 = path;

		strcpy_s(oldFile, path.c_str());
		_mkdir(oldFile);
		path2 += "/";
		path2 += "versions";
		strcpy_s(tempFile, path2.c_str());
		_mkdir(tempFile);
	}



}
string Notepad::common::returnFilepath(string userName, string projectName, int choice) {
	if (choice == 1) {


		string path = userName;
		path += "/projects/";
		path += projectName;
		path += "/data.txt";
		return path;
	}
	if (choice == 3) {
		string path = userName;
		path += "/projects/";
		path += projectName;
		path += "/temp.txt";
		return path;
	}
	if (choice == 4) {
		string path = userName;
		path += "/projects/";
		path += projectName;
		path += "/versions/";
		return path;
	}

}
string Notepad::common::fileInput() {
	string newLine;
	cout << "Enter line to append \n";

	getline(cin, newLine);
	newLine += "\n";
	return newLine;


}

bool Notepad::common::checkFile(string path) {
	ifstream fin;
	fin.open(path);
	if (fin) {
		fin.close();

		return 1;
	}
	else
		return 0;

}
