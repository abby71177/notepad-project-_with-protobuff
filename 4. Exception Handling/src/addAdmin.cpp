#include "userAuthentication.pb.h"
#include"Notepad.h"

void user::userObj:: addAdmin()
{
    user::userObj users_;
    user::userDetails *obj;
    obj = users_.add_users();
    obj->set_admin(true);
    obj->set_name("Admin");
    obj->set_username("admin");
    string encrypted = encryptDecryptPass("admin");
    obj->set_password(encrypted);
    obj->set_email("admin@gmail.com");
    obj->set_location("Chennai");
    obj->set_no_of_projects(0);
    fstream output("userDetails.dat", ios::out | ios::trunc | ios::binary);
    if (!users_.SerializeToOstream(&output)) 
        cerr << "Failed to write address book." << endl;
    else {
        _mkdir("admin");
        Notepad::common::createDirectory("admin", "null", 1);
        cout << "<------------ Admin added successfully ------------>\n";
    }


}
