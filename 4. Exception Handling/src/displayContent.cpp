#include "projectDetails.pb.h"
#include "userAuthentication.pb.h"
#include "notepad.h"

void projectData::projectDetails ::displayContents(char* directory) {
    fstream newfile;
    string  projectName;
    int lineNo = 1;

    cin.get();
    cout << "Enter the Project name  eg.(Project 1/2/...)  ::";
    getline(cin, projectName);
    string path("");
    path = Notepad::common::returnFilepath(directory, projectName, 1);
    bool result = Notepad::common::checkFile(path);
    if (result==1) {
        newfile.open(path, ios::in);

        string tp;
        while (getline(newfile, tp)) {
            cout << lineNo << " " << tp << "\n";
            lineNo++;
        }
        newfile.close();

    }
    else
        cout << "No such file exists\n";



}

void projectData::projectDetails::userDashboard(char* user) {
    int found = 0;
    projectDetails project_;
    fstream input("project.dat", ios::in | ios::binary);
    if (!project_.ParseFromIstream(&input))
        throw invalid_argument("File not found -project");
    else
    {
        cout << "\n\nProjects created by User - " << user << "\n\n";

        for (int index = 0;index < project_.projects_size();index++)
        {
            project obj = project_.projects(index);
            if (obj.username() == user)
            {
                found = 1;
                cout << "\Project Id :: " << obj.id();
                cout << "\nProject Name :: " << obj.projectname();
                cout << "\nNo of Operations done so far :: " << obj.no_of_operations();
                //cout << "\nNo of Versions of this project  created so far ::" << projectObj.no_of_versions << "\n\n";
                cout << "\n<--- Versions created for this project - " << obj.projectname() << " --->";
                displayVersions(2, user, obj.projectname());
                cout << endl << endl << endl;
            }

        }
        if (found == 0)
            cout << "\nProjects are not yet created by this user\n";
    }
}


void projectData::projectDetails::projectInfo(char* directory) {               // to display project details of a particular product
    int found = 0;
    string file;

    cin.get();
    cout << "\nEnter Project Id :: ";
    getline(cin, file);
    string project = directory;
    project += "/";
    project += file;

    char newPath[20];
    strcpy_s(newPath, project.c_str());
    ifstream filein;
    projectDetails project_;
    user::userObj users_;
    user::userDetails obj;
    
    fstream input("project.dat", ios::in | ios::binary);
    if (!project_.ParseFromIstream(&input))
        throw invalid_argument("File not found -project");
    else
    {

        for (int index = 0;index < project_.projects_size();index++)
        {
            projectData::project obj = project_.projects(index);
            if (obj.username() == directory && obj.id() == file)
            {
                found = 1;
                cout << "\Project Id :: " << obj.id();
                cout << "\nProject Name :: " << obj.projectname();
                cout << "\nNo of Operations done so far :: " << obj.no_of_operations();
                cout << "\nNo of Versions of this project  created so far ::" << obj.no_of_versions() << "\n\n";
            }

        }
        if (found == 0)
            cout << "\nProjects are not yet created by this user\n";
    }
}

        
