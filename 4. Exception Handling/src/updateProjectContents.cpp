#include "projectDetails.pb.h"
#include "Notepad.h"

void projectData:: projectDetails:: updateContent(char* directory)
{         //To Update Contents Of the Project either overwrite or update particular line
    int choice, lineNo, lineCount;
    string line, updateLine, replaceLine, projectName;
    string path("");
    cin.get();
    cout << "Enter Project name :: ";
    getline(cin, projectName);
    path = Notepad::common::returnFilepath(directory, projectName, 1);
    int result = Notepad::common::checkFile(path);
    if (result==1)
    {
        cout << "\nTo Update Contents Of the Project\n1.To overwrite the entire project\n2.To update a particular line\nenter choice ::";
        cin >> choice;

        if (choice == 1) {
            cin.get();
            ofstream ofs(path, ofstream::trunc);
            cout << "Enter line to overwrite the file with :: ";
            getline(cin, line);

            ofs << line << "\n";
            ofs.close();
            updateOperations(projectName, directory, 1);
            checkVersion(projectName, directory);
        }
        else {
            cout << "Enter line number you wish to be updated :: ";
            cin >> lineNo;
            cin.get();
            cout << "Enter line the line to be replaced with :: ";
            getline(cin, replaceLine);
            string path2("");


            path2 = Notepad::common::returnFilepath(directory, projectName, 3);

            ifstream fin;
            fin.open(path);
            ofstream temp;
            temp.open(path2);

            lineCount = 1;
            while (getline(fin, line) && fin)
            {
                //line.replace(line.find(deleteline), deleteline.length(), "");
                if (lineCount != lineNo)
                    temp << line << "\n";
                else if (lineCount == lineNo)
                    temp << replaceLine << "\n";
                lineCount++;
            }
            if (lineNo >= lineCount || lineNo <= 0)
                cout << "\nRequested line is not present in the file\n";
            else
            {
                cout << "\nRequested line was updated\n";

                temp.close();
                fin.close();
                char newFile[50], oldFile[50];
                strcpy_s(oldFile, path.c_str());
                strcpy_s(oldFile, path.c_str());


                strcpy_s(newFile, path2.c_str());
                const char* n = oldFile;
                const char* n1 = newFile;
                const int res = remove(n);
                const char* name = "data.txt";
                const int res2 = rename(n1, n);
                //cout << res<<endl << res2;
                updateOperations(projectName, directory, 1);
                checkVersion(projectName, directory);
            }

        }
    }

    else
        cout << "No such file exists\n";


}
